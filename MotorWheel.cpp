#include "Arduino.h"
#include "MotorWheel.h"

MotorWheel::MotorWheel(float _stepAngle, int _pin0, int _pin1, int _pin2, int _pin3, Mode _modeForward, Mode _modeBackward, Print& _print)
                      : stepAngle(_stepAngle), pin0(_pin0), pin1(_pin1), pin2(_pin2), pin3(_pin3), modeForward(_modeForward), modeBackward(_modeBackward), printer(_print)
{
  pinMode(this->pin0, OUTPUT);
  pinMode(this->pin1, OUTPUT);
  pinMode(this->pin2, OUTPUT);
  pinMode(this->pin3, OUTPUT);
}

MotorWheel::MotorWheel(float _stepAngle, int _pin0, int _pin1, int _pin2, int _pin3, Mode _modeForward, Print& _print)
: MotorWheel(_stepAngle, _pin0, _pin1, _pin2, _pin3, _modeForward, _modeForward == Mode::CLOCKWISE ? Mode::COUNTER_CLOCKWISE : Mode::CLOCKWISE, _print) {

}

void MotorWheel::forward() {
  this->mode = this->modeForward;
}
void MotorWheel::backward() {
  this->mode = this->modeBackward;
}

void MotorWheel::stop() {
  if (this->mode != Mode::STOPPED) {
    this->writePins(LOW, LOW, LOW, LOW);
    this->mode = Mode::STOPPED;
  }
}
void MotorWheel::setSpeed(float rotationsPerSecond){
  this->rotationsPerSecond = rotationsPerSecond;
  int fullStepsForFullRotation = 360 / this->stepAngle; // 4096 for a 28byj-48 ?

  this->secondPerStep = ((float)1 / this->rotationsPerSecond) / (float)fullStepsForFullRotation;
  printer.print("secondPerStep = ");
  printer.println(secondPerStep, 10);
  if (secondPerStep < 0.001) {
    printer.print("Be careful, a time inferior to 1 millisecond between each step is hard to obtain, so update() might skip some steps, therefore losing speed.");
  }
}
void MotorWheel::update(float elapsedTime) {
  if (this->mode == Mode::STOPPED) {
    return;
  }
  static float totalTime = 0;
  totalTime += elapsedTime;
  if (totalTime > secondPerStep) {
    if (this->mode == Mode::COUNTER_CLOCKWISE) {
      this->phase -= 1;
      if (this->phase < 0) {
        this->phase = 7;
      }
    } else if (this->mode == Mode::CLOCKWISE) {
      this->phase += 1;
      if (this->phase > 7) {
        this->phase = 0;
      }
    }
    this->phaseSelect(phase);
    totalTime -= elapsedTime;
    if (totalTime > secondPerStep) {
      totalTime = 0;
      // Can't handle that speed.
    }
  }
}
void MotorWheel::phaseSelect(int phase)
{
  switch(phase){
     case 0:
       this->writePins(LOW,LOW,LOW,HIGH);
       break;
     case 1:
       this->writePins(LOW,LOW,HIGH,HIGH);
       break;
     case 2:
       this->writePins(LOW,LOW,HIGH,LOW);
       break;
     case 3:
       this->writePins(LOW,HIGH,HIGH,LOW);
       break;
     case 4:
       this->writePins(LOW,HIGH,LOW,LOW);
       break;
     case 5:
       this->writePins(HIGH,HIGH,LOW,LOW);
       break;
     case 6:
       this->writePins(HIGH,LOW,LOW,LOW);
       break;
     case 7:
       this->writePins(HIGH,LOW,LOW,HIGH);
       break;
     default:
       this->writePins(LOW,LOW,LOW,LOW);
       break;
  }
}

void MotorWheel::writePins(int pinValue0, int pinValue1, int pinValue2, int pinValue3) {
  digitalWrite(this->pin0, pinValue0);
  digitalWrite(this->pin1, pinValue1);
  digitalWrite(this->pin2, pinValue2);
  digitalWrite(this->pin3, pinValue3);
}
