#ifndef MOTOR_WHEEL_
# define MOTOR_WHEEL_

class MotorWheel {

  public:

  typedef enum Mode {
    STOPPED,
    CLOCKWISE,
    COUNTER_CLOCKWISE
  } Mode;

  MotorWheel(float stepAngle, int driverPin0, int driverPin1, int driverPin2, int driverPin3, Mode forward, Print&);
  void forward();
  void backward();
  void stop();
  void setSpeed(float rotationsPerSecond);
  void update(float elapsedTime);

  private:

  MotorWheel(float _stepAngle, int _pin0, int _pin1, int _pin2, int _pin3, Mode _modeForward, Mode _modeBackward, Print&);

  void phaseSelect(int phase);
  void writePins(int pinValue0, int pinValue1, int pinValue2, int pinValue3);

  Mode mode = Mode::STOPPED;
  int phase = 0;
  float rotationsPerSecond;
  float secondPerStep;

  const float stepAngle;
  const int pin0;
  const int pin1;
  const int pin2;
  const int pin3;
  const Mode modeForward;
  const Mode modeBackward;
  Print& printer;
};

#endif
